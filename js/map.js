function myMap() {
  var e = document.getElementById("map");
  var myCenter = new google.maps.LatLng(10.4614721, 105.5299186);
  var option = {
    center: myCenter,
    zoom: 17
  };
  var map = new google.maps.Map(e, option);

  var marker = new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE
  });
  marker.setMap(map);

  var infoWindow = new google.maps.InfoWindow({
    content: "I'm here"
  });

  infoWindow.open(map, marker);
}
