$(document).ready(function() {
  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    dotsClass: false,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      500: {
        items: 2
      },
      1100: {
        items: 3
      }
      // 1902: {
      //   items: 6
      //aa
      // }
    }
  });

  var countElement = document.getElementsByClassName("background").length;
  var time = 4000;
  function doBackground() {
    var backgrounds = document.getElementsByClassName("background");

    for (let i = 0; i < backgrounds.length; i++) {
      const background = backgrounds[i];
      setTimeout(() => {
        clearActiveClass();
        clearAnimationBanner();
        background.classList.add("activeBackground");
        background.children[0].classList.add("m_FadeRightToLeft");
        // m_FadeLeftToRight
      }, time * i);
    }
  }

  function clearActiveClass() {
    var backgrounds = document.getElementsByClassName("background");
    for (let i = 0; i < backgrounds.length; i++) {
      const background = backgrounds[i];
      if (background.classList.contains("activeBackground")) {
        background.classList.remove("activeBackground");
      }
    }
  }
  function clearAnimationBanner() {
    var banners = document.getElementsByClassName("bannerContent");
    for (let i = 0; i < banners.length; i++) {
      const banner = banners[i];
      if (banner.classList.contains("m_FadeRightToLeft")) {
        banner.classList.remove("m_FadeRightToLeft");
      }
    }
  }
  setInterval(() => {
    doBackground();
  }, countElement * time);
  doBackground();
  new fullpage("#fullpage", {
    responsiveWidth: 768,
    responsiveHeight: 600
  });
});
